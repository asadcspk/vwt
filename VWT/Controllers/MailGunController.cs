﻿using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using VWT.Models;

namespace VWT.Controllers
{
    public class MailGunController : SurfaceController
    {



        [System.Web.Http.HttpPost]
        public JsonResult Save(MailGunModel model)
        {
            try
            {

                //var results = AddListMember(model);
                SendSimpleMessage(model);
                //if (results.StatusCode == System.Net.HttpStatusCode.BadRequest)
                //{
                //    return Json(new { status = false, data = "" }, JsonRequestBehavior.AllowGet);
                //}
                //if (results.ResponseStatus == RestSharp.ResponseStatus.Completed)
                //{
                //    var data = SendSimpleMessage(model);
                //    var d = SendadminMessage(model);
                //    return Json(new { status = true, data = "" }, JsonRequestBehavior.AllowGet);
                //}
                //else
                //{
                //    return Json(new { status = false, data = "" }, JsonRequestBehavior.AllowGet);
                //}
                return Json(new { status = true, data = "" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                return Json(new { status = false, data = "" }, JsonRequestBehavior.AllowGet);
            }

        }
        public IRestResponse AddListMember(MailGunModel model)
        {
            RestClient client = new RestClient();
            client.BaseUrl = new Uri("https://api.mailgun.net/v3");
            client.Authenticator =
                new HttpBasicAuthenticator("api",
                                            "8517b9087b627d943e4506b960b5652d-8889127d-e6afd8d9");
            RestRequest request = new RestRequest();
            request.Resource = "lists/{list}/members";
            request.AddParameter("list", "contact@mail.veinandwoundtraining.com",
                                  ParameterType.UrlSegment);
            request.AddParameter("address", model.Address);
            request.AddParameter("subscribed", true);
            request.AddParameter("name", model.Name);
            request.AddParameter("vars", model.Parameter);
            request.Method = Method.POST;
            return client.Execute(request);
        }

        public static IRestResponse SendSimpleMessage(MailGunModel model)
        {
            string html = "<table><tbody><tr><td> Thank you contacting for contacting us @ veinandwoundtraining.com. Though we try our best to respond to messages submitted from our site and call back within 24 business hours. If you do not hear back from us within this time period, please call us on <a href='tel:(978) 666-4200'>(978) 666-4200.</a>  </td></tr>";
            html += "<tr><td>&nbsp;</td></tr><tr><td>We are committed to hands on training in vein and wound care. Our courses aim to equip attendees with the knowledge which can be applied in their day to day practice  to perform them vein and wound treatments with more confidence. After course attendees can ask us relevant questions by emailing pictures or questions. We also offer on-site hand holding for physicians beginning their career in vein and wound and be happy to be part of your exceptional business mentorship to succeed. </td></tr>";
            html += "<tr><td>&nbsp;</td></tr><tr><td>We will look forward to serving you and be part of this unique educational partnership. </td></tr>";
            html += "<tr><td>&nbsp;</td></tr><tr><td>With Best Regards,</td></tr>";
            html += "<tr><td>Team at Vein and Wound Training Institute</td></tr>";
            html += "<tr><td>978 Worcester St, Unit 2 Wellesley, MA 02482</td></tr>";
            html += "<tr><td>T (978) 666-4200</td></tr>";
            html += "<tr><td>F (888) 561-3002</td></tr>";
            html += "<tr><td>info@veinandwoundtraining.com</td></tr>";
            html += "<tr><td>&nbsp;</td></tr><tr><td>Please use email to communicate and do not reply to this message, because we will not receive reply to this message.</td></tr>";
            html += "</tbody></table>";
            RestClient client = new RestClient();
            client.BaseUrl = new Uri("https://api.mailgun.net/v3");
            client.Authenticator =
                new HttpBasicAuthenticator("api",
                                            "8517b9087b627d943e4506b960b5652d-8889127d-e6afd8d9");
            RestRequest request = new RestRequest();
            request.AddParameter("domain", "mail.veinandwoundtraining.com", ParameterType.UrlSegment);
            request.Resource = "{domain}/messages";
            request.AddParameter("from", "Vein & Wound <postmaster@mail.veinandwoundtraining.com> ");
            request.AddParameter("to", model.Address);
            request.AddParameter("subject", "Thankyou for reaching us");
            request.AddParameter("html", html);

            request.Method = Method.POST;
            return client.Execute(request);
        }

        public static IRestResponse SendadminMessage(MailGunModel model)
        {
            string html = "<table><tbody><tr><td>Name:" + model.Name + "</td></tr>";
            html += "<tr><td>Phone:" + model.Phone + "</td></tr>";
            html += "<tr><td>Email:" + model.Address + "</td></tr>";
            if (!string.IsNullOrEmpty(model.Message))
            {
                html += "<tr><td>Message:" + model.Message + "</td></tr>";
            }
            if (!string.IsNullOrEmpty(model.Office))
            {
                html += "<tr><td>Office:" + model.Office + "</td></tr>";
            }
            if (!string.IsNullOrEmpty(model.Date))
            {
                html += "<tr><td>Date:" + model.Date + "</td></tr>";
            }
            html += "</tbody></table>";
            RestClient client = new RestClient();
            client.BaseUrl = new Uri("https://api.mailgun.net/v3");
            client.Authenticator =
                new HttpBasicAuthenticator("api",
                                            "8517b9087b627d943e4506b960b5652d-8889127d-e6afd8d9");
            RestRequest request = new RestRequest();
            request.AddParameter("domain", "mail.uniquehealth.org", ParameterType.UrlSegment);
            request.Resource = "{domain}/messages";
            request.AddParameter("from", "Uniquehealth <postmaster@mail.uniquehealth.org>");
            request.AddParameter("to", "mhabib@uniquehealth.org");
            request.AddParameter("subject", "Unique Health Message -" + model.PageTitle);
            request.AddParameter("html", html);

            request.Method = Method.POST;
            return client.Execute(request);
        }

    }
}