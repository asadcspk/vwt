﻿using System;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using VWT.Models;

namespace VWT.Controllers
{
    public class ContactUsController : SurfaceController
    {

        [HttpPost]
        public JsonResult Send(ContactUsModel model)
        {
            try
            {

                try
                {
                    //var root = Umbraco.ContentAtRoot().FirstOrDefault();
                    //string html = "<table><tbody><tr><td>Name:" + model.Name + "</td></tr>";
                    //html += "<tr><td>Email:" + model.Email + "</td></tr>";
                    //html += "<tr><td>Phone:" + model.Phone + "</td></tr>";
                    //html += "<tr><td>Message:" + model.Message + "</td></tr>";
                    //html += "</tbody></table>";
                    //MailMessage message = new MailMessage();
                    //SmtpClient smtp = new SmtpClient();
                    //message.From = new MailAddress("admin@morshandel.dk");
                    //message.To.Add(root.Value<string>("contactEmail"));
                    //message.Subject = "Contact";
                    //message.IsBodyHtml = true; //to make message body as html  
                    //message.Body = html;
                    //smtp.Host = "asmtp.onlinemail.io"; //for gmail host  
                    //smtp.EnableSsl = true;
                    //smtp.UseDefaultCredentials = false;
                    //smtp.Credentials = new NetworkCredential("admin@morshandel.dk", "Admin1234!");
                    //smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                    //smtp.Send(message);
                    return Json(new { status = true, data = model }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { status = false, data = ex.Message }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception)
            {
                return Json(new { status = false, data = "" }, JsonRequestBehavior.AllowGet);
            }

        }




    }
}