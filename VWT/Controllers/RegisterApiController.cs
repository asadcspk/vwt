﻿using System.Collections.Generic;
using System.Linq;
using Umbraco.Web.Editors;
using VWT.Models;

namespace VWT.Controllers
{
    public class RegisterApiController : UmbracoAuthorizedJsonController
    {

        public IEnumerable<RegisterModel> GetAll()
        {
            return Enumerable.Empty<RegisterModel>();
        }
    }
}