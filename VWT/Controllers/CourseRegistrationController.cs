﻿using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Web.Mvc;
using Umbraco.Core.Scoping;
using Umbraco.Web.Mvc;
using VWT.Repositories.PetaPocoDB;
using VWT.Models;
using VWT.Services;

namespace VWT.Controllers
{
    public class CourseRegistrationController : SurfaceController
    {
        private readonly IScopeProvider _scopeProvider;

        public CourseRegistrationController(IScopeProvider scopeProvider)
        {
            this._scopeProvider = scopeProvider;
        }

        [HttpPost]
        public JsonResult Register(RegisterModel model)
        {
            

                try
                {
                    model.RegistrationDate = DateTime.Now;
                    model.GuId = Guid.NewGuid();
                    model.PaymentStatus = "Pending";
                    var _respository = new RegistrationRepository();
                    _respository.Add(model);
                //var _service = new RegistrationService(_scopeProvider);
                //    _service.Add(model);
                    var respone = AddListMember(model);
                    return Json(new { status = true, data = model }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { status = false, data = ex.Message }, JsonRequestBehavior.AllowGet);
                }

           

        }


        public IRestResponse AddListMember(RegisterModel model)
        {
            RestClient client = new RestClient();
            string jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(model);
            client.BaseUrl = new Uri("https://api.mailgun.net/v3");
            client.Authenticator =
                new HttpBasicAuthenticator("api",
                                            "8517b9087b627d943e4506b960b5652d-8889127d-e6afd8d9");
            RestRequest request = new RestRequest();
            request.Resource = "lists/{list}/members";
            request.AddParameter("list", "enroll@mail.veinandwoundtraining.com",
                                  ParameterType.UrlSegment);
            request.AddParameter("address", model.Email);
            request.AddParameter("subscribed", true);
            request.AddParameter("name", model.FirstName);
            request.AddParameter("vars", jsonString);
            request.Method = Method.POST;
            return client.Execute(request);
        }


        public JsonResult UpdateStatus(string guId)
        {
            try
            {
                var _respository = new RegistrationRepository();
                _respository.Update(guId);
                var response = SendSimpleMessage(guId);
                return Json(new { status = true, Data = "Updated" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = false, Data = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }


        public  IRestResponse SendSimpleMessage(string guId)
        {
            var _respository = new RegistrationRepository();
            var data= _respository.Get(guId);
            string html = "<table><tbody><tr><td>Thank you for registering for "+ data.CourseName + ". We are committed to hands on training in vein and wound care education. We will be contacting you shortly with dates of your course (if not already advertised on our site) for mutually convenient days for training.</td></tr>";
            html += "<tr><td>Please do not hesitate to contact us by emailing or calling us. </td></tr>";
            html += "<tr><td>With Best Regards,</td></tr>";
            html += "<tr><td>&nbsp;</td></tr><tr><td>Muzzamal Habib, MD & Rest of Team @ Vein & Wound Training Institute</td></tr>";
            html += "<tr><td>978 Worcester St, #2</td></tr>";
            html += "<tr><td>Wellesley, MA 02482</td></tr>";
            html += "<tr><td>978-666-4200 ext 10</td></tr>";
            html += "<tr><td>info@veinandwoundtraining.com</td></tr>";
            html += "</tbody></table>";
            RestClient client = new RestClient();
            client.BaseUrl = new Uri("https://api.mailgun.net/v3");
            client.Authenticator =
                new HttpBasicAuthenticator("api",
                                            "8517b9087b627d943e4506b960b5652d-8889127d-e6afd8d9");
            RestRequest request = new RestRequest();
            request.AddParameter("domain", "mail.veinandwoundtraining.com", ParameterType.UrlSegment);
            request.Resource = "{domain}/messages";
            request.AddParameter("from", "Vein & Wound <postmaster@mail.veinandwoundtraining.com> ");
            request.AddParameter("to", data.Email);
            request.AddParameter("subject", "Thankyou for registring");
            request.AddParameter("html", html);

            request.Method = Method.POST;
            return client.Execute(request);


        }

    }
}