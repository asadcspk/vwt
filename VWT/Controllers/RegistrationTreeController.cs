﻿using System;
using Umbraco.Core;
using Umbraco.Web;
using Umbraco.Web.Models.Trees;
using Umbraco.Web.Mvc;
using Umbraco.Web.Trees;

namespace VWT.Tree
{
    [Tree("Section", "registraion", TreeGroup = "", TreeTitle = "Registraion List", SortOrder = 1)]
    [PluginController("RegistrationListView")]
    public class RegistrationTreeController : TreeController
    {
        protected override Umbraco.Web.Models.Trees.MenuItemCollection GetMenuForNode(string id, System.Net.Http.Formatting.FormDataCollection queryStrings)
        {
            return null;
        }

        protected override Umbraco.Web.Models.Trees.TreeNodeCollection GetTreeNodes(string id, System.Net.Http.Formatting.FormDataCollection queryStrings)
        {
            //check if we're rendering the root node's children
            if (id == "-1")
            {
                var nodes = new TreeNodeCollection();
                nodes.Add(CreateTreeNode("registraion", id, queryStrings, "Registraions", "icon-people", false, FormDataCollectionExtensions.GetValue<string>(queryStrings, "application") + StringExtensions.EnsureStartsWith(this.TreeAlias, '/') + "/registraions/all"));
                return nodes;
            }

            //this tree doesn't suport rendering more than 2 levels
            throw new NotSupportedException();
        }
    }
}