﻿


using PetaPoco;
using VWT.Models;


namespace VWT.Repositories.PetaPocoDB
{
  public  class RegistrationRepository
    {
        readonly Database _db = new Database("umbracoDbDSN");
        public void Add(RegisterModel model)
        {
            _db.Insert(model);
        }
        public void Update(RegisterModel model)
        {
            _db.Update(model);
           

        }
        public void Update(string guId)
        {
            var model = Get(guId);
            model.PaymentStatus = "Paid";
            Update(model);

        }
        public RegisterModel Get(int id)
        {
         
            return _db.SingleOrDefault<RegisterModel>("SELECT * FROM vwRegistrations WHERE Id =@0", id);

        }

        public RegisterModel Get(string guId)
        {

            return _db.SingleOrDefault<RegisterModel>("SELECT * FROM vwRegistrations WHERE GuId =@0", guId);

        }
        public int Delete(int id)
        {
            return _db.Execute("Delete vwRegistrations WHERE Id = @0", id);
        }
        
       
    }
}
