﻿using System.Linq;
using Umbraco.Core.Persistence;
using Umbraco.Core.Scoping;
using VWT.Models;

namespace VWT.Services
{
    public class RegistrationService
    {
        private readonly IScopeProvider _scopeProvider;

        public RegistrationService(IScopeProvider scopeProvider)
        {
            this._scopeProvider = scopeProvider;
        }
        public void Add(RegisterModel model)
        {
            using (var scope = _scopeProvider.CreateScope(autoComplete: true))
            {

                var result = scope.Database.Insert("vwRegistrations", null, model);

                string name = "";
               
            }

        }
        public RegisterModel GetList(int id)
        {
            using (var scope = _scopeProvider.CreateScope(autoComplete: true))
            {
                var sql = scope.SqlContext.Sql()
                    .Select("*")
                    .Where<RegisterModel>(x => x.Id == id);

                return scope.Database.SingleOrDefault<RegisterModel>(sql);
            }
        }
        public PagedResult GetAll(int page, int pageSize)
        {
            using (var scope = _scopeProvider.CreateScope(autoComplete: true))
            {
                var sql = scope.SqlContext.Sql().Select("*").From("vwRegistrations").OrderBy("Id");

                //var data = scope.Database.Fetch<RegisterModel>(sql);
                var p = scope.Database.Page<RegisterModel>(page, pageSize, sql);
                var result = new PagedResult
                {
                    TotalPages = p.TotalPages,
                    TotalItems = p.TotalItems,
                    ItemsPerPage = p.ItemsPerPage,
                    CurrentPage = p.CurrentPage,
                    Records = p.Items.ToList()
                };
                return result;
            }
        }

        public void DoSomething(RegisterModel model)
        {
            using (var scope = _scopeProvider.CreateScope())
            {
                // Scope.Database has what you need/want
                //scope.Database.Fetch<RegisterModel>("Select * From MyTable");
                scope.Database.Insert(model);
                // You must always complete a scope
                scope.Complete();
            }
        }

        public void Update(string guId)
        {
            using (var scope = _scopeProvider.CreateScope(autoComplete: true))
            {
                var sql = scope.SqlContext.Sql()
                    .Select("*")
                    .Where<RegisterModel>(x => x.GuId.ToString() == guId);

                var data = scope.Database.SingleOrDefault<RegisterModel>(sql);
                data.PaymentStatus = "Paid";
                scope.Database.Update(data);
            }
        }

        public RegisterModel Get(string guId)
        {
            using (var scope = _scopeProvider.CreateScope(autoComplete: true))
            {
                var sql = scope.SqlContext.Sql()
                    .Select("*")
                    .Where<RegisterModel>(x => x.GuId.ToString() == guId);

                return scope.Database.SingleOrDefault<RegisterModel>(sql);
            }
        }
    }
}