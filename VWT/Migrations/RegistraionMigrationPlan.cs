﻿using Umbraco.Core.Migrations;
using VWT.Models;

namespace VWT.Migrations
{
    public class RegistraionMigrationPlan : MigrationPlan
    {
        public RegistraionMigrationPlan()
            : base("RegistrationsDatabaseTable")
        {
            From(string.Empty)
                .To<MigrationCreateTables>("registration-migration");
        }
    }
    public class MigrationCreateTables : MigrationBase
    {
        public MigrationCreateTables(IMigrationContext context)
            : base(context)
        {

        }

        public override void Migrate()
        {
            if (!TableExists("vwRegistrations"))
            {
                Create.Table<RegisterModel>().Do();
            }
        }
    }
}