﻿namespace VWT.Models
{
    public class ContactUsModel
    {

        public string FirstName { get; set; }
        public string Phone { get; set; }
        public string LasttName { get; set; }
        public string Message { get; set; }

    }
}