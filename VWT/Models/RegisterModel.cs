﻿
using System;


namespace VWT.Models
{
    

    [PetaPoco.TableName("vwRegistrations")]
    [PetaPoco.PrimaryKey("Id")]
    public class RegisterModel
    {

        public int Id { get; set; }
        public Guid GuId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string Experience { get; set; }
        public string CourseName { get; set; }
        public DateTime RegistrationDate { get; set; }
        public string PaymentStatus { get; set; }
        public string Physion { get; set; }
        public double Amount { get; set; }

        public string Address2 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string ZIP { get; set; }

        public string Country { get; set; }
    }
}