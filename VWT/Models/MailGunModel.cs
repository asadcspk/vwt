﻿namespace VWT.Models
{
    public class MailGunModel
    {
        public string Address { get; set; }
        public string Name { get; set; }

        public string Parameter { get; set; }

        public string Phone { get; set; }
        public string Message { get; set; }
        public string Office { get; set; }
        public string Date { get; set; }
        public string PageTitle { get; set; }
    }
}