﻿using System.Collections.Generic;

namespace VWT.Models
{
    public class PagedResult
    {
        public List<RegisterModel> Records { get; set; }


        public long CurrentPage { get; set; }


        public long ItemsPerPage { get; set; }


        public long TotalPages { get; set; }


        public long TotalItems { get; set; }
    }
}