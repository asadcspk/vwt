﻿using NPoco;
using Umbraco.Core.Persistence.DatabaseAnnotations;

namespace VWT.Models
{
    [TableName("MyCustomDatabaseTable")]
    [PrimaryKey("Id", AutoIncrement = true)]
    [ExplicitColumns]
    public class MyCustomDatabaseModel
    {
        [Column("id")]
        [PrimaryKeyColumn(AutoIncrement = true)]
        public int Id { get; set; }

        [Column("MyCustomColumn")]
        public string MyCustomColumn { get; set; }
    }
}