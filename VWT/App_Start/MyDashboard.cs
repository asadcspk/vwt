﻿using Umbraco.Core.Composing;
using Umbraco.Core.Dashboards;

namespace VWT.App_Start
{
    [Weight(-10)]
    public class MyDashboard : IDashboard
    {
        public string Alias => "myCustomDashboard";

        public string[] Sections => new[] { "content", "settings" };

        public string View => "/App_Plugins/myCustom/dashboard.html";

        public IAccessRule[] AccessRules
        {
            get
            {
                var rules = new IAccessRule[]
                {
                    new AccessRule {Type = AccessRuleType.Deny, Value = Umbraco.Core.Constants.Security.TranslatorGroupAlias},
                    new AccessRule {Type = AccessRuleType.Grant, Value = Umbraco.Core.Constants.Security.AdminGroupAlias}
                };
                return rules;
            }
        }
    }
}