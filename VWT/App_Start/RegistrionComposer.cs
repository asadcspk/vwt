﻿

using Umbraco.Core;
using Umbraco.Core.Composing;
using VWT.Services;

namespace VWT.App_Start
{
    public class RegistrionComposer : IUserComposer
    {
        public void Compose(Composition composition)
        {
            composition.Components()
                .Append<RegistraionComponent>();
            composition.Register<RegistrationService>();
        }
    }
}